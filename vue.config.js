module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    proxy: 'https://omnicellwebapi2.azurewebsites.net/',
}
}
