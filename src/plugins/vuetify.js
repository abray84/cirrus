import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);
Vue.use(VueRouter);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: colors.green.darken1,
        secondary: colors.grey.darken4,
        accent: colors.teal.darken1,
      },
      dark: {
        primary: colors.green.darken1,
        secondary: colors.grey,
        accent: colors.green.darken2,
      },
    }
  }
});
