import Vue from 'vue'
import VueRouter from 'vue-router';
import vuetify from './plugins/vuetify';

import App from './App.vue'
import routes from './routes';
import VuePdfApp from "vue-pdf-app";
import "vue-pdf-app/dist/icons/main.css";

const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.config.productionTip = false
Vue.component("vue-pdf-app", VuePdfApp);

new Vue({
  router,
  vuetify,
  mounted: function () {
    const theme = localStorage.getItem("darkTheme");
    if (theme) {
      if (theme === "true") {
          this.$vuetify.theme.dark = true;
      } else {
          this.$vuetify.theme.dark = false;
      }
    }
    else {
      this.$vuetify.theme.dark = true;
    }
  },
  render: h => h(App)
}).$mount('#app')
