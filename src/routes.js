import Home from './views/Home.vue';
import Demo from './views/Demo.vue';
import Pdf from './views/Pdf.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/demo', component: Demo },
    { path: '/pdf', component: Pdf },
];

export default routes;